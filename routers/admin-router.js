const express = require('express');

const router = express.Router();

const adminController = require('../controllers/admin-controller');

router.post('/create-menu', adminController.createMenu);

router.get('/find-menu', adminController.getOneMenu);

router.get('/find-all-menu', adminController.getAllMenu);

router.put('/update-menu', adminController.updateMenu);

router.delete('/delete-menu', adminController.deleteOneMenu);

module.exports = router;