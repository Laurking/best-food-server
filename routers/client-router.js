const express = require('express');

const router = express.Router();

const clientController = require('../controllers/client-controller');

router.get('/find-menu', clientController.getOneMenu);

router.get('/find-all-menu', clientController.getAllMenu);

module.exports  = router;