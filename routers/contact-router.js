const express = require('express');

const router = express.Router();

const contactController = require('../controllers/contact-controller');

const ContactValidator = require('../utils/contact-form-validator');



router.post('/send-message', 
[ ContactValidator.validateFullName(),
    ContactValidator.validateEmail(),
    ContactValidator.validateSubject(),
    ContactValidator.validateMessage()
],
contactController.sendMessage);

router.get('/find-message', contactController.getAllMessages);

router.delete('/remove-message', contactController.deleteOneMessage);

router.delete('/remove-all-message', contactController.deleteAllMessages);

module.exports  = router;