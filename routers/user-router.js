const express = require('express');

const router = express.Router();
const AuthValidator = require('../utils/auth-validator');

const userController = require('../controllers/user-controller');

router.post('/signup',[
    AuthValidator.validateSignupEmail(),
    AuthValidator.validateSignupPassword(),
    AuthValidator.validateConfirmPassword()
], userController.signup);

router.post('/signin', [
    AuthValidator.validateSigninEmail(),
    AuthValidator.validateSigninPassword()
], userController.signin);

router.put('/update-user', userController.updateUser);

router.post('/delete-user', userController.deleteUser);

router.put('/forgot-pasword', userController.postForgotPassword);

router.put('/validate-reset-token', userController.validatePasswordResetToken);

router.put('/update-password', userController.postNewPassword);

module.exports  = router;