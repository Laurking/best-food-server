exports.pageNotFound=(request, response, next)=>{
    return response.status(404)
            .json({message:'Page not found', statusCode: 404})
}

exports.serverError=(error, request, response, next)=>{
    return response.status(500)
            .json({message:'Internal Server Error', statusCode: 500})
}