const Contact = require('../models/contact');
const Mailer = require('../mailer/mailer-client');
const constants = require('../constants/variables');
const NoteTemplate = require('../mailer/templates');
const Normalizer = require('../utils/data-field-normalizer');

const { validationResult } = require('express-validator/check');

exports.sendMessage = async(request, response, next)=>{
   const fullname = request.body.fullname;
   const email = request.body.email;
   const subjectvalue = request.body.subject;
   const messageValue = request.body.message;
  //console.log({email: email, fullname:fullname, subject:subject, message:message});
   const errorArrays = validationResult(request);
   if(!errorArrays.isEmpty()){
      //console.log(errorArrays.array());
      return response.status(302).json({
         message: 'error',
         statusCode:401,
         oldInputValue:{fullname:fullname, email:email, subject:subjectvalue, message:messageValue},
         errorMessages: errorArrays.array()
      });  
   }
  
   try{
      const contact = new Contact({
         fullname: new Normalizer(fullname).normalizeString().capitalizeFullName().getStringValue(),
         email: new Normalizer(email).normalizeString().toLoweCase().getStringValue(),
         subject: new Normalizer(subjectvalue).normalizeString().capitalize().getStringValue(),
         message: new Normalizer(messageValue).normalizeString().getStringValue(),
      });
      const savedContact = await contact.save()
      const subject = 'Thank you for contacting us';
      const message = NoteTemplate.ThankYouNoteFromContact;
      Mailer.sendMail(constants.customerService, savedContact.email, subject, message);
      return response.status(200).json({
         message: 'success',
         statusCode:200,
         data:fullname
      });
   }catch(err){
      console.log(err);
      const error = new Error(err);
      error.httpStatusCode = 500;
      return next(error);
   }
}

exports.getAllMessages = (request, response, next)=>{
   console.log("remove one menu");
}


exports.deleteOneMessage = (request, response, next)=>{
   console.log("remove one menu");
}


exports.deleteAllMessages = (request, response, next)=>{
   console.log("remove one menu");
}

exports.getSearch = (request, response, next) =>{
   return response.render('shop/pages/search',{
      currentPage:'search',
      title: 'Search'
   });
}

exports.postSearch = (request, response, next) =>{
   return response.render('shop/pages/search',{
      currentPage:'search',
      title: 'Search'
   });
}

