const User = require('../models/user');
const Bcrypt = require('bcryptjs');
const constants = require('../constants/variables');
const Mailer = require('../mailer/mailer-client');
const NoteTemplate = require('../mailer/templates');
const TokenGenerator = require('../utils/token-generator');
const Normalizer = require('../utils/data-field-normalizer');
const { validationResult } = require('express-validator/check');

exports.signup = async(request, response, next)=>{
    const email = request.body.email;
    const password = request.body.password;
    const confirmPassword = request.body.confirmPassword;
    try{
        const numberOfUsers = await User.countDocuments({});
        if(numberOfUsers > 0){
            return response.json({message:'error', statusCode:422, data:'Please contact admin'});
        }
    }catch(err){
        const error = new Error(err);
        error.httpStatusCode = 500;
        return next(error);
    }
    
    const errorArrays = validationResult(request);
    if(!errorArrays.isEmpty()){
        return response.json({
            message:'error',
            statusCode:422,
            error: errorArrays.array(),
            data:{ email: email, password: password, confirmPassword: confirmPassword }
        });
    }
    try{
        const hashedPassword = await Bcrypt.hash(password, 12)
        const user =  new User({email: new Normalizer(email).normalizeString().toLoweCase().getStringValue(), password:hashedPassword});
        await user.save();
        return response.status(200).json({ message:'successfully signed up',statusCode:200,});
    }catch(err){
        const error = new Error(err);
        error.httpStatusCode = 500;
        return next(error);
    }
}

exports.signin = async (request, response, next)=>{
    const email = request.body.email;
    const password = request.body.password;
    console.log({email:email, password:password});
    // const errorArrays = validationResult(request);
    // if(!errorArrays.isEmpty()){
    //     return response.status(422).render('shop/pages/signin',{
    //         currentPage:'signin', title: 'Signin',
    //         errorMessages: errorArrays.array(),
    //         oldInputValue:{ email: email, password: password }
    //     });
    // }
    // const user = await User.findOne({
    //     email: new Normalizer(email).normalizeString().toLoweCase().getStringValue()
    // })
    // const doMatch = await Bcrypt.compare(password, user.password);
    // try{
    //     if (doMatch) {
    //         request.session.isAuthenticated = true; request.session.user = user;
    //         request.session.save();
    //         return response.redirect('shop/products');
    //     }
    //     return response.render('shop/pages/signin',{
    //         currentPage:'signin', title: 'Signin',
    //         errorMessages: [ { msg: 'Invalid email or password' } ],
    //         oldInputValue:{ email: email, password: password } 
    //     })
    // } catch(err) {
    //     const error = new Error(err);
    //     error.httpStatusCode = 500;
    //     return next(error);
    // }
}

exports.updateUser = (request, response, next)=>{
    console.log("remove one menu");
}

exports.deleteUser = (request, response, next)=>{
    console.log("remove one menu");
}

exports.postForgotPassword = async(request, response, next)=>{
    const email = request.body.email;
    const errorArrays = validationResult(request);
    if(!errorArrays.isEmpty()){
        return response.render('shop/pages/forgot-password',{
            currentPage: 'forgot password', title:'Forgot password',
            oldInputValue:{email: email}, errorMessages: errorArrays.array()
        });
    }
    try{
        let user = await User.findOne({email: new Normalizer(email).normalizeString().toLoweCase().getStringValue()})
        user.token.value = TokenGenerator.generateToken();
        user.token.expiresAt = Date.now()+3600000;
        user.save();
        const subject = 'Please follow the below instructions to change your password';
        const message = NoteTemplate.resetPassword(user.token.value);
        Mailer.sendMail(constants.customerService, user.email, subject, message);
        return response.render('shop/pages/password-change-token-sent',{
            currentPage:'password-change', title:'Password change information'
        });
    }catch(err){
        const error = new Error(err);
        error.httpStatusCode = 500;
        return next(error);
    }
}

exports.validatePasswordResetToken = async(request, response, next)=>{
    const token = request.params.token;
    try{
        const user = await User.findOne({'token.value':token, 'token.expiresAt':{$gt: new Date()}})
        if(!user){
            return response.render('shop/pages/forgot-password',{
                currentPage: 'forgot password', title:'Forgot password',
                errorMessages:[{ location: 'body',param: 'email',msg: 'This request expired. Please try again' }],
                oldInputValue:{email: ''}
            });
        }
        return response.render('shop/pages/new-password', {
            currentPage: 'new password', title:'New password',
            oldInputValue:{ password: '', confirmPassword: ''},
            errorMessages:[],
            userId: user._id
        })  
    }catch(err){
        const error = new Error(err);
        error.httpStatusCode = 500;
        return next(error);
    }
}

exports.postNewPassword = async (request, response, next)=>{
    const password = request.body.password;
    const confirmPassword = request.body.confirmPassword;
    const userId = request.body.userId;
    const errorArrays = validationResult(request);
    if(!errorArrays.isEmpty()){
        return response.render('shop/pages/new-password',{
            currentPage: 'new password', title:'New password',
            errorMessages: errorArrays.array(), userId: userId,
            oldInputValue:{ password: password, confirmPassword: confirmPassword}
        })
    }
    try{
        const user = await User.findOne({_id:userId})
        const hashedPassword = await Bcrypt.hash(password,12);
        user.oldPasswords.push(user.password);
        user.password = hashedPassword;
        user.token.value = undefined;
        user.token.expiresAt = undefined;
        user.save();
        return response.redirect('/shop/users/signin');            
    }catch(err ){
        const error = new Error(err);
        error.httpStatusCode = 500;
        return next(error);
    }
}

exports.getLogout = async(request, response, next)=>{
    try{
        await request.session.destroy();
        return  response.redirect('/shop/users/signin');
    }catch(err){
        const error = new Error(err);
        error.httpStatusCode = 500;
        return next(error);
    }
}

exports.submitNewPassword = (request, response, next)=>{
    console.log("submmit password one menu");
}



