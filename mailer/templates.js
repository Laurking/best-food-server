exports.ThankYouNoteFromContact = `
<div class="container" style="width: 50%; margin:.5em 0px; padding: 10px; overflow: hidden;">
<h1 style="text-align: center; font-size: 1.2em; margin:.5em 0px 3em 0px;">Thank you for contacting us</h1>
<p>Your email is important to us. Our Customer Care Center will work with you shortly.</p>
<p>Very best,</p>
<h4>
<div>Shop 24/7 Team Support</div>
<div>Email: <span style="font-size: .8em; color: blue;">shopping.shop@twentyfour.com</span></div>
<div>Phone: <span style="font-size: .8em; color: blue;">1(212) 555-5555</span></div>
</h4>
</div>
`;


exports.resetPassword = (token)=>{
	return `<h1 style="font-size: 1.2em; text-align: center;">**PLEASE DO NOT RESPOND TO THIS E-MAIL**</h1>
	<h2 style="text-align: center; color: red;">Password Reset Request Action Needed</h2>
	<div class="container" style="padding-left: 20px;">A request to reset the password associated with this email address has been received. Please click on the link shown which will connect you to our secure server. You will then be given instructions for resetting your password.
	<div class="button" style="text-align: center;"><a href='http://localhost:3000/shop/users/reset-password/${token}' style="width: 200px; margin: 1.5em auto; line-height: 20px; font-size: 1em; display: block; text-decoration: none; color: #fff; outline: 2px solid #3399ff; cursor: pointer; background-color: #17a2b8; border-color: #17a2b8; padding: 5px 0px 7px 0px;">Reset Password</a></div>
	<div class="info" style="padding-top: 15px;">
	Please contact our office immediately if you did not initiate this change or if you have any questions.
	</div>
	<div class="info" style="padding-top: 15px;">Shop 24/7,</div>
	<div>Email: <span>shopping.shop@twentyfour.com</span></div>
	</div>`
};
