// core library imports 
const path = require('path');
const express = require('express');
const cors = require('cors');
const bodyParser = require('body-parser');
const cookieParser = require('cookie-parser');
const session = require('express-session');
const MongoDBStore = require('connect-mongodb-session')(session);
const mongoose = require('mongoose');

// import routers
const adminRouter = require('./routers/admin-router');
const clientRouter = require('./routers/client-router');
const contactRouter = require('./routers/contact-router');
const userRouter = require('./routers/user-router');
const errorRouter = require('./routers/errors-router');

// constant variables imports
const constantVariables = require('./constants/variables');

const app = express();

app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json({ type: 'application/json' }));

const store = new MongoDBStore({
    uri: constantVariables.dbConnectionUrl, 
    collection:'session'
})
app.use(cookieParser());
app.use(session({
    secret: "my secret",
    resave: false,
    saveUninitialized: false,
    store: store
}));

const corsOptions = {
    origin:'http://localhost:4200',
    withCredentials:false
}
app.use(cors(corsOptions));


// setting routes 

app.use('/admin',adminRouter);
app.use('/client',clientRouter);
app.use('/contact',contactRouter);
app.use('/users',userRouter);
app.use(errorRouter);
// app.use((error, request, response, next)=>{
//     return response.status(500)
//     .render('shop/pages/500',{
//         currentPage:'500',
//          title: '500 ERROR'
//      });
// })

mongoose.connect(constantVariables.dbConnectionUrl)
.then(connection =>{
    app.listen(3000);
    console.log('connected');
})
.catch(error =>{
    console.log(error);
});

console.log("started listening on http://localhost:3000");