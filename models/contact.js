const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const contactSchema = new Schema({
    fullname:{
        type:String, 
        required:true
    },
    email: {
        type: String, 
        required: true
    },
    subject: { 
        type: String,
        required: true
    },
    message: { 
        type: String,
        required: true
    },
    created_at: Date
});


module.exports = mongoose.model('Contact', contactSchema);