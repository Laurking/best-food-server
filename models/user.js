const mongoose = require('mongoose');
const Bcrypt = require('bcryptjs');
const Schema = mongoose.Schema;

const userSchema = new Schema({
    firstname:{
        type:String, 
        required:false
    },
    lastname:{
        type:String, 
        required:false
    },
    email: {
        type: String, 
        unique:true,
        required: true, 
    },
    password: { 
        type: String,
        required: true
    },
    token: {
        value:{
            type: String,
            required: false
        },
        expiresAt: {
            type: Date,
            required: false
        }
    },
    created_at: Date,
    updated_at: Date
});

userSchema.methods.comparePassword = function(password, callback) {
    Bcrypt.compare(password, this.password, function(err, isMatch) {
      if (err) return callback(err);
      callback(null, isMatch);
    });
  };

module.exports = mongoose.model('User', userSchema);
