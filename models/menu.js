const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const menuSchema = new Schema({
    name:{
        type:String, 
        required:true
    },
    price: {
        type: String, 
        required: true
    },
    images:[
        {data:string, required:true}
    ],
    description: { 
        type: String,
        required: true
    },
    created_at: Date
});


module.exports = mongoose.model('Menu', menuSchema);