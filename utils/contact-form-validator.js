const { body } = require('express-validator/check');
const Contact = require('../models/contact');

module.exports = class ContactValidator{
    
    static validateFullName(){
        return body('fullname')
                .isLength({min:1}).withMessage('field must not be empty')
                .matches(/^[a-zA-Z]([-']?[a-zA-Z]+)*( [a-zA-Z]([-']?[a-zA-Z]+)*)+$/,"i").withMessage("require valid first name  space last name");
    }

    static validateEmail(){
        return body('email')
                .isLength({min:1}).withMessage('field must not be empty')
                .isEmail().withMessage('email must be valid');
    }

    static validateSubject(){
        return body('subject')
                .isLength({min:1}).withMessage('field must not be empty')
                .isLength({min:2}).withMessage('subject must be at least one word')
                .isLength({max:50}).withMessage('subject is too long (50 max)');
    }

    static validateMessage(){
        return body('message')
                .isLength({min:1}).withMessage('field must not be empty')
                .isLength({min:50, max:500}).withMessage('expect between 50 and 200 characters');
    }
}