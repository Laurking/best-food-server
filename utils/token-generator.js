const Bcrypt = require('crypto');

module.exports = class Hash{
    static generateToken(){
        return Bcrypt.randomBytes(32).toString('hex');
    }
}
