const { body } = require('express-validator/check');
const User = require('../models/user');
const Normalizer = require('./data-field-normalizer');
module.exports = class AuthValidator{
    
    static validateSigninEmail(){
        return body('email')
        .isLength({min:1})
        .withMessage("field cannot be empty")
        .isEmail()
        .withMessage("require valid email")
        .custom(value => {
            return User.findOne({email: new Normalizer(value).normalizeString().toLoweCase().getStringValue()})
            .then(user => {
                if (!user) {
                    return Promise.reject('Invalid email or password');
                }
            });
        })
    }
    
    static validateSigninPassword(){
        return body('password')
        .isLength({min: 1})
        .withMessage("field cannot be empty");
    }
    
    static validateSignupEmail(){
        return body('email')
        .isLength({min:1})
        .withMessage("field cannot be empty")
        .isEmail()
        .withMessage("Invalid email format")
        .custom(value => {
            return User.findOne({email: new Normalizer(value).normalizeString().toLoweCase().getStringValue()})
            .then(user => {
                if (user) {
                    return Promise.reject('email already exists');
                }
            });
        })
    }
    
    static validateSignupPassword(){
        return body('password')
        .isLength({min:1})
        .withMessage('field cannot be empty')
        .isLength({min:8, max:20})
        .withMessage("expects 8 to 20 characters");
    }
    
    static validateConfirmPassword(){
        return  body('confirmPassword')
        .isLength({min:1})
        .withMessage('field cannot be empty')
        .custom(( value, { req }) => {
            if (value !== req.body.password) {
                return Promise.reject('passwords must match');
            } 
            return true;
        });
    }
}