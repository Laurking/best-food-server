const { body } = require('express-validator/check');
const User = require('../models/user');
const Normalizer = require('./data-field-normalizer');
const Bcryt = require('bcryptjs');


module.exports = class StandardValidator{
    
    static validateEmail(){
        return body('email')
        .isLength({min:1})
        .withMessage("field cannot be empty")
        .isEmail()
        .withMessage("require valid email")
        .custom(value => {
            return User.findOne({email: new Normalizer(value).normalizeString().toLoweCase().getStringValue()})
            .then(user => {
                if (!user) {
                    return Promise.reject('There is no such user with this email');
                }
            });
        })
    }
    
    
    static validatePassword(){
        return body('password')
        .isLength({min:1})
        .withMessage('field cannot be empty')
        .isLength({min:8, max:20})
        .withMessage("expects 8 to 20 characters");
    }
    
    static validateConfirmPassword(){
        return  body('confirmPassword')
        .isLength({min:1})
        .withMessage('field cannot be empty')
        .custom(( value, { req }) => {
            if (value !== req.body.password) {
                return Promise.reject('Confirmation must match password');
            } 
            return true;
        });
    }
    
     static oldPasswordCheck(){
        return body('userId')
        .custom(( value, { req })=>{
            return User.findOne({_id: value})
            .then(user => {
                if (user) {
                   return Bcryt.compare(req.body.password, user.password)
                    .then(isMatch =>{
                        if(isMatch) return Promise.reject(new Error('Old passwords not accepted'));
                    });
                }
                return true;
            });
        });
    }
    
    // static oldPasswordCheck(){
    //     return body('userId')
    //     .custom(( value, { req })=>{
    //         return User.findOne({_id: value})
    //         .then(user => {
    //             if (user) {
    //                return Bcryt.compare(req.body.password, user.password)
    //                 .then(isMatch =>{
    //                     if(isMatch) return Promise.reject(new Error('Old passwords not accepted'));
    //                 });
    //             }
    //             return true;
    //         });
    //     });
    // }
    
}